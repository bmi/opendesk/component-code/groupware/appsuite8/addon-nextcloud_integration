/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.nextcloud.access.token.provider.impl;

import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.nextcloud.oauth.NextcloudLoginAndWebdavUrlProvider;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;

@NonNullByDefault
public class GetTokenAction implements AJAXActionService {

    private final NextcloudLoginAndWebdavUrlProvider urlProvider;
    private final LeanConfigurationService           leanConf;

    public GetTokenAction(NextcloudLoginAndWebdavUrlProvider urlProvider, LeanConfigurationService leanConf) {
        super();
        this.urlProvider = urlProvider;
        this.leanConf = leanConf;
    }

    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        final boolean includeAccessToken = leanConf.getBooleanProperty(session.getUserId(), session.getContextId(),
            DefaultProperty.valueOf("com.openexchange.nextcloud.filepicker.includeAccessToken", true));
        final String accessToken = getAccessTokenFromSession(session);
        final JSONObject ret = new JSONObject().putSafe("login", urlProvider.getLogin(session, accessToken));
        if (includeAccessToken) {
            ret.putSafe("accessToken", accessToken);
        }
        return new AJAXRequestResult(ret);
    }

    protected String getAccessTokenFromSession(ServerSession session) throws OXException {
        Object parameter = session.getParameter(Session.PARAM_OAUTH_ACCESS_TOKEN);
        if (null == parameter) {
            throw OXException.general("no token present in session");
        }
        if (parameter instanceof String) {
            return (String) parameter;
        }
        throw OXException.general("token present but in wrong type in session");
    }
}
