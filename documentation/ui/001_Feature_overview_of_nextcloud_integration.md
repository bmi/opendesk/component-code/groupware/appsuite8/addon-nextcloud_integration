# Feature overview of the OX App Suite integration with Nextcloud

## Motivation
Nextcloud customers who need a powerful groupware solution can now share files even more easily with OX App Suite. OX App Suite customers who do not want to miss out on the familiar features in Nextcloud now have the option to work with both products.

## Supported Use Cases
OX App Suite now integrates with Nextcloud and allows the following use cases:

- Email attachments can be easily selected and attached from Nextcloud via a file picker.
- Files from Nextcloud can also be inserted into an email as sharing link. The user can optionally specify a password, write permissions and expiration date for the links at the bottom of the file picker. Afterwards, the link settings can be managed in Nextcloud.
- The file picker allows to navigate the Nextcloud folder tree. Clicking a folder shows the folder content. The breadcrumb allows to navigate back in the folder tree.
- Multiple selection is supported to insert more than one link or attachment at once.
- Files attached to an email in OX App Suite can be saved in Nextcloud. The file picker allows to select the folder in the Nextcloud folder tree where the uploaded attachments are stored.
