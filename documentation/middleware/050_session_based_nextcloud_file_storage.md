# Session based Nextcloud File Storage

|||
| --------------------------------- | -------------------------------------------------------------------------------- |
| Story for original implementation | [NCLD-1](https://jira.open-xchange.com/browse/NCLD-1), [NCLD-8](https://jira.open-xchange.com/browse/NCLD-8)
| Code repository                   | [extensions/nextcloud-integration](https://gitlab.open-xchange.com/extensions/nextcloud-integration)
| Bundle Identifier                 | `com.openexchange.file.storage.nextcloud.oauth`
| Package(s)                        | `open-xchange-nextcloud-oauth-session`
| Required capabilities             | `com.openexchange.capability.filestorage_nextcloud_oauth=true`
| Available since                   | 7.10.5-rev1
| Maintainers                       | Carsten Hoeger


The Nextcloud file picker allows to use `access_tokens` bound to the session to create Nextcloud accounts on the fly.

## Configuration

`/opt/open-xchange/etc/nextcloud_oauth.properties`

```
# The Nextcloud endpoint for automatic account creation
# Should be in the format https://host/nextcloud that reflects the base url of the Nextcloud System
# Default: <empty>
com.openexchange.file.storage.nextcloud.oauth.url=

# Strategy to get the username of the /remote.php/dav/files/USERNAME/
#   Possible options:
#     * username - use the provisioned username of the user in ox
#     * user_status - fetches the userID from the nextcloud system
#                     using /ocs/v2.php/apps/user_status/api/v1/user_status
#                     (DEPRECATED! -- see NCLD-36)
#     * user - fetches the userID from the Nextcloud system,
#              using /ocs/v1.php/cloud/user
# ConfigCascade aware.
#
# Default: username
com.openexchange.file.storage.nextcloud.oauth.webdav.username.strategy=username

# The folder name shown in the OX Infostore
# Default: Nextcloud
com.openexchange.file.storage.nextcloud.oauth.displayName=Nextcloud

# The capability to enable the automatic handling of Nextcloud session based accounts
# Default: not_set
com.openexchange.capability.filestorage_nextcloud_oauth=

# The capability filestorage_nextcloud must also be enabled
# Default: not_set
com.openexchange.capability.filestorage_nextcloud=

# Should the /api/nextcloud/accesstoken JSON response contain the accessToken key and value?
# ConfigCascade aware.
#
# Default: true
com.openexchange.nextcloud.filepicker.includeAccessToken=true
```
