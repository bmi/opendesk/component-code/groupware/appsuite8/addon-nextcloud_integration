/// <reference types='codeceptjs' />

type steps_file = typeof import('@codeceptjs/configure/test/integration/steps_file.js');
type users = typeof import('@open-xchange/codecept-helper/src/users.js');
type contexts = typeof import('@open-xchange/codecept-helper/src/contexts.js');
type myExampleApp = typeof import('./pageobjects/my-example-app');
type OpenXchange = import('./helper');
type MockRequestHelper = import('@codeceptjs/mock-request');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any, users: users, contexts: contexts, myExampleApp: myExampleApp }
  interface Methods extends Puppeteer, OpenXchange, FileSystem, MockRequestHelper {}
  interface I extends ReturnType<steps_file>, WithTranslation<OpenXchange>, WithTranslation<FileSystem>, WithTranslation<MockRequestHelper> {}
  namespace Translation {
    interface Actions {}
  }
}
