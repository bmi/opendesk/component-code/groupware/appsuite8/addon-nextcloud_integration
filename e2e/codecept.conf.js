require('dotenv-defaults').config()

const requiredEnvVars = ['LAUNCH_URL', 'PROVISIONING_URL', 'CONTEXT_ID']

requiredEnvVars.forEach(function notdefined (key) {
  if (process.env[key]) return
  console.error('\x1b[31m', `ERROR: Missing value for environment variable '${key}'. Please specify a '.env' file analog to '.env-example'.`)
  process.exit()
})

const helpers = {
  Puppeteer: {
    url: process.env.LAUNCH_URL,
    smartWait: 1000,
    waitForTimeout: Number(process.env.WAIT_TIMEOUT),
    browser: 'chrome',
    restart: true,
    windowSize: '1280x1024',
    uniqueScreenshotNames: true,
    timeouts: {
      script: 5000
    },
    chrome: {
      executablePath: process.env.CHROME_BIN,
      args: [
        `--unsafely-treat-insecure-origin-as-secure=${process.env.LAUNCH_URL}`,
        '--kiosk-printing',
        '--disable-web-security',
        '--disable-dev-shm-usage',
        '--disable-gpu',
        '--disable-setuid-sandbox',
        '--no-sandbox',
        '--no-first-run'
      ].concat((process.env.CHROME_ARGS || '').split(' '))
    },
    // set HEADLESS=false in your terminal to show chrome window
    show: process.env.HEADLESS ? process.env.HEADLESS === 'false' : false,
    waitForNavigation: ['domcontentloaded', 'networkidle0']
  },
  OpenXchange: {
    require: './helper',
    mxDomain: process.env.MX_DOMAIN,
    serverURL: process.env.PROVISIONING_URL,
    contextId: process.env.CONTEXT_ID,
    filestoreId: process.env.FILESTORE_ID,
    smtpServer: process.env.SMTP_SERVER,
    imapServer: process.env.IMAP_SERVER,
    loginTimeout: 30,
    admin: {
      login: process.env.E2E_ADMIN_USER,
      password: process.env.E2E_ADMIN_PW
    }
  },
  FileSystem: {},
  MockRequestHelper: {
    require: '@codeceptjs/mock-request'
  }
}

module.exports.config = {
  tests: './tests/**/*_test.js',
  timeout: 90,
  output: './output/',
  helpers,
  include: {
    I: './actor',
    users: '@open-xchange/codecept-helper/src/users.js',
    contexts: '@open-xchange/codecept-helper/src/contexts.js',
    // pageobjects
    myExampleApp: './pageobjects/my-example-app'
  },
  bootstrap: async () => {
    const codecept = require('codeceptjs')
    const config = codecept.config.get()
    const helperConfig = config.helpers.OpenXchange

    const contexts = codecept.container.support('contexts')
    // eslint-disable-next-line
    const helper = new ((require('@open-xchange/codecept-helper')).helper)()
    const testRunContext = await contexts.create()
    if (typeof testRunContext.id !== 'undefined') helperConfig.contextId = testRunContext.id
  },
  teardown: async function () {
    const { contexts } = global.inject()
    // we need to run this sequentially, less stress on the MW
    for (const ctx of contexts.filter(ctx => ctx.id > 100)) {
      if (ctx.id !== 10) await ctx.remove().catch(e => console.error(e.message))
    }
  },
  reporter: 'mocha-multi',
  mocha: {
    reporterOptions: {
      'codeceptjs-cli-reporter': {
        stdout: '-'
      },
      'mocha-junit-reporter': {
        stdout: '-',
        options: {
          jenkinsMode: true,
          mochaFile: './output/junit.xml',
          attachments: false // add screenshot for a failed test
        }
      }
    }
  },
  plugins: {
    allure: { enabled: true, require: '@codeceptjs/allure-legacy' },
    browserLogReport: {
      require: '@open-xchange/allure-browser-log-report',
      enabled: true
    },
    customizePlugin: {
      require: process.env.CUSTOMIZE_PLUGIN || '@open-xchange/codecept-helper/src/plugins/emptyModule',
      enabled: true
    },
    // leave this empty, we only want this plugin to be enabled on demand by a developer
    pauseOnFail: {}
  },
  rerun: {
    minSuccess: Number(process.env.MIN_SUCCESS),
    maxReruns: Number(process.env.MAX_RERUNS)
  },
  name: 'App Suite Example Plugin'
}
