%define __jar_repack %{nil}

Name:          open-xchange-nextcloud-filepicker
BuildArch:     noarch
%if 0%{?rhel_version} && 0%{?rhel_version} >= 700
BuildRequires: ant
%else
BuildRequires: ant-nodeps
%endif
%if 0%{?suse_version}
BuildRequires: java-1_8_0-openjdk-devel
%else
BuildRequires: java-1.8.0-openjdk-devel
%endif
BuildRequires: open-xchange-core >= @OXVERSION@, open-xchange-core < @NEXTMINOR@
BuildRequires: open-xchange-nextcloud-oauth-session >= @OXVERSION@, open-xchange-nextcloud-oauth-session < @NEXTMINOR@
Version:       @OXVERSION@
%define        ox_release 0
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       Module to interact with nextcloud filepicker
Autoreqprov:   no
Requires:      open-xchange-core >= @OXVERSION@, open-xchange-core < @NEXTMINOR@
Requires:      open-xchange-nextcloud-oauth-session >= @OXVERSION@, open-xchange-nextcloud-oauth-session < @NEXTMINOR@
Obsoletes:     open-xchange-custom-filepicker
Conflicts:     open-xchange-custom-filepicker

%description
This package provides an endpoints to interact with nextcloud filepicker methods

Authors:
--------
    Open-Xchange

%prep

%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/bundles/
/opt/open-xchange/bundles/*
%dir /opt/open-xchange/osgi/bundle.d/
/opt/open-xchange/osgi/bundle.d/*
%dir /opt/open-xchange/i18n/
/opt/open-xchange/i18n/*

%changelog
