package com.openexchange.file.storage.nextcloud.oauth.impl;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;

public class NextcloudResponseParserTest {

    private static InputStream load(String name) throws FileNotFoundException {
        String[] pkg = NextcloudResponseParserTest.class.getPackage().getName().split("\\.");
        String[] resourceName = new String[pkg.length + 1];
        System.arraycopy(pkg, 0, resourceName, 0, pkg.length);
        resourceName[resourceName.length - 1] = name;
        Path resource = Paths.get("test", resourceName);
        return new FileInputStream(resource.toFile());
    }
    
    @Test
    public void testOcsDataIdWorks() throws Exception {
        try (InputStream is = load("user1.json")) {
            assertThat(NextcloudResponseParser.ocsDataId(is)).isEqualTo("bb793e1c-5778-4089-8f00-2a5095ae5970");
        }
    }
    
    @Test
    public void testOcsDataUserIdWorks() throws Exception {
        try (InputStream is = load("user_status1.json")) {
            assertThat(NextcloudResponseParser.ocsDataUserId(is)).isEqualTo("bb793e1c-5778-4089-8f00-2a5095ae5970");
        }
    }

}
