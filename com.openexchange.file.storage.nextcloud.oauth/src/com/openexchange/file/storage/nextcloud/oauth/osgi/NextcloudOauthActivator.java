/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth.osgi;

import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.file.storage.FileStorageAccountManagerProvider;
import com.openexchange.file.storage.nextcloud.oauth.NextcloudLoginAndWebdavUrlProvider;
import com.openexchange.file.storage.nextcloud.oauth.impl.NextcloudOauthFileStorageAccountManagerProvider;
import com.openexchange.file.storage.nextcloud.oauth.impl.NextcloudOauthHttpCLientConfigProvider;
import com.openexchange.file.storage.nextcloud.oauth.impl.NextcloudLoginAndWebdavUrlProviderImpl;
import com.openexchange.file.storage.registry.FileStorageServiceRegistry;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.SpecificHttpClientConfigProvider;

/**
 * {@link NextcloudOauthActivator}
 *
 * @author <a href="mailto:felix.marx@open-xchange.com">Felix Marx</a>
 * @since v7.10.5
 */
@NonNullByDefault
public class NextcloudOauthActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] {
            LeanConfigurationService.class,
            CapabilityService.class,
            FileStorageServiceRegistry.class,
            HttpClientService.class,
        };
    }

    @Override
    protected void startBundle() throws Exception {
        NextcloudOauthFileStorageAccountManagerTracker tracker = new NextcloudOauthFileStorageAccountManagerTracker(context);
        rememberTracker(tracker);
        openTrackers();
        NextcloudLoginAndWebdavUrlProviderImpl urlProvider = new NextcloudLoginAndWebdavUrlProviderImpl(getServiceSafe(LeanConfigurationService.class), getServiceSafe(HttpClientService.class));
        registerService(NextcloudLoginAndWebdavUrlProvider.class, urlProvider);
        registerService(ForcedReloadable.class, urlProvider);
        FileStorageAccountManagerProvider accountManagerProvider = new NextcloudOauthFileStorageAccountManagerProvider(
            tracker,
            getServiceSafe(FileStorageServiceRegistry.class),
            getServiceSafe(LeanConfigurationService.class),
            getServiceSafe(CapabilityService.class),
            urlProvider);
        registerService(SpecificHttpClientConfigProvider.class, new NextcloudOauthHttpCLientConfigProvider(getServiceSafe(LeanConfigurationService.class)));
        registerService(FileStorageAccountManagerProvider.class, accountManagerProvider);
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

}
