/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth;

import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.exception.OXException;
import com.openexchange.session.Session;

@NonNullByDefault
public interface NextcloudLoginAndWebdavUrlProvider {

    /**
     * Returns the login for Nextcloud for a given accessToken
     * 
     * @param session The session of the user
     * @param accessToken The accessToken of the user
     * @return The full webdav Url of the user
     * @throws OXException in case the url could not be created
     */
    public String getLogin(Session session, String accessToken) throws OXException;

    /**
     * Returns the full webdav Url for a given accessToken
     * 
     * @param session The session of the user
     * @param accessToken The accessToken of the user
     * @return The full webdav Url of the user
     * @throws OXException in case the url could not be created
     */
    public String getUrl(Session session, String accessToken) throws OXException;

}
