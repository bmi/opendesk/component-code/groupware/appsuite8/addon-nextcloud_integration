/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth.impl;

import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.slf4j.Logger;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.ForcedReloadable;
import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.nextcloud.oauth.N;
import com.openexchange.file.storage.nextcloud.oauth.NextcloudLoginAndWebdavUrlProvider;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.HttpClients;
import com.openexchange.rest.client.httpclient.ManagedHttpClient;
import com.openexchange.session.Session;
import com.openexchange.session.UserAndContext;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.webdav.client.BearerAuthScheme;

@NonNullByDefault
public class NextcloudLoginAndWebdavUrlProviderImpl implements NextcloudLoginAndWebdavUrlProvider, ForcedReloadable {

    private static final String USER_ENDPOINT_PATH        = "ocs/v1.php/cloud/user";
    private static final String USER_STATUS_ENDPOINT_PATH = "ocs/v2.php/apps/user_status/api/v1/user_status";
    
    private final Cache<UserAndContext, String> urlCache;
    private final Cache<UserAndContext, String> loginCache;
    private final LeanConfigurationService      leanConf;
    private final HttpClientService             httpClientService;

    private static final Logger LOG = N.logger(NextcloudLoginAndWebdavUrlProviderImpl.class);

    public NextcloudLoginAndWebdavUrlProviderImpl(LeanConfigurationService leanConf, HttpClientService httpClientService) {
        super();
        this.leanConf = leanConf;
        this.httpClientService = httpClientService;
        this.loginCache = N.notNull(CacheBuilder.newBuilder()
            .expireAfterAccess(15, TimeUnit.MINUTES)
            .expireAfterWrite(60, TimeUnit.MINUTES)
            .build());
        this.urlCache = N.notNull(CacheBuilder.newBuilder()
            .expireAfterAccess(15, TimeUnit.MINUTES)
            .expireAfterWrite(60, TimeUnit.MINUTES)
            .build());
    }

    private static interface IdExtractor {
        String extract(InputStream is) throws Exception;
    }
    
    protected String getFromNextcloud(Session session, String accessToken, String path, IdExtractor extractor) throws OXException {
        ManagedHttpClient httpClient = getClient();
        HttpGet get = null;
        HttpResponse response = null;
        try {
            String host = getBase(session) + path;
            get = new HttpGet(host);
            get.setHeader("OCS-APIRequest", "true");
            get.setHeader("Accept", "application/json");
            setupBearerAuth(get, accessToken);
            response = httpClient.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 100 || statusCode == 200) {
                try (InputStream inputStream = Streams.bufferedInputStreamFor(response.getEntity().getContent())) {
                    return extractor.extract(inputStream);
                }
            }
            if (statusCode == 401) {
                throw NextcloudOauthExceptionCodes.AUTH_FAILED.create("401 returned from Nextcloud");
            }
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("Status code returned " + statusCode);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw NextcloudOauthExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage(), e);
        } finally {
            HttpClients.close(get, response);
        }
    }

    @Override
    public String getUrl(Session session, String accessToken) throws OXException {
        try {
            return urlCache.get(UserAndContext.newInstance(session), () -> {
                return getWebdavBase(session) + getLogin(session, accessToken) + "/";
            });
        } catch (ExecutionException e) {
            LOG.error("Could not fetch url", e);
            throw NextcloudOauthExceptionCodes.UNEXPECTED_ERROR.create("Internal communication");
        }
    }

    @Override
    public String getLogin(Session session, String accessToken) throws OXException {
        try {
            return loginCache.get(UserAndContext.newInstance(session), () -> {
                switch (getStrategy(session)) {
                    case USERNAME:
                        return ServerSessionAdapter.valueOf(session).getUser().getLoginInfo();
                    case USER_STATUS:
                        return getFromNextcloud(session, accessToken, USER_STATUS_ENDPOINT_PATH, NextcloudResponseParser::ocsDataUserId);
                    case USER:
                        // NCLD-36
                        return getFromNextcloud(session, accessToken, USER_ENDPOINT_PATH, NextcloudResponseParser::ocsDataId);
                    default:
                        break;
                }
                throw NextcloudOauthExceptionCodes.UNEXPECTED_ERROR.create("could not get a configured url");
            });
        } catch (ExecutionException e) {
            LOG.error("Could not fetch url", e);
            throw NextcloudOauthExceptionCodes.UNEXPECTED_ERROR.create("Internal communication");
        }
    }

    protected String getWebdavBase(Session session) throws OXException {
        return getBase(session) + "remote.php/dav/files/";
    }
    
    protected String getBase(Session session) throws OXException {
        String base = leanConf.getProperty(session.getUserId(), session.getContextId(), DefaultProperty.valueOf("com.openexchange.file.storage.nextcloud.oauth.url", ""));
        if (null == base || Strings.isEmpty(base)) {
            throw NextcloudOauthExceptionCodes.HOST_NOT_CONFIGURED.create();
        }
        if (!base.endsWith("/")) {
            base += "/";
        }
        return base;
    }

    protected NextcloudWebdavStrategy getStrategy(Session session) throws OXException {
        String usernameStrategy = leanConf.getProperty(session.getUserId(), session.getContextId(), DefaultProperty.valueOf("com.openexchange.file.storage.nextcloud.oauth.webdav.username.strategy", "username"));
        if (null == usernameStrategy || Strings.isEmpty(usernameStrategy)) {
            return NextcloudWebdavStrategy.USERNAME;
        }
        try {
            return NextcloudWebdavStrategy.valueOf(usernameStrategy.toUpperCase());
        } catch (Exception e) {
            LOG.debug("could not fetch a Strategy with name {}, using USERNAME instead", usernameStrategy);
            return NextcloudWebdavStrategy.USERNAME;
        }
    }

    protected ManagedHttpClient getClient() {
        return httpClientService.getHttpClient(NextcloudOauthHttpCLientConfigProvider.CLIENTID);
    }

    private void setupBearerAuth(HttpRequestBase request, String token) throws OXException {
        try {
            Header authenticate = new BearerAuthScheme().authenticate(new UsernamePasswordCredentials(token, null), request);
            request.addHeader(authenticate);
        } catch (AuthenticationException e) {
            throw NextcloudOauthExceptionCodes.AUTH_FAILED.create(e.getMessage(), e);
        }
    }

    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        loginCache.invalidateAll();
        urlCache.invalidateAll();
    }

}
