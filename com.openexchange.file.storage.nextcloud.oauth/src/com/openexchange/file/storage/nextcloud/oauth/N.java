/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.annotation.Nullable;

/**
 * Helper class used in the Nextcloud components
 * {@link N}
 *
 * @author <a href="mailto:felix.marx@open-xchange.com">Felix Marx</a>
 */
@NonNullByDefault
public enum N {
    ;

    @SuppressWarnings("null")
    public static Logger logger(final Class<?> c) {
        return LoggerFactory.getLogger(c);
    }

    public static <X> X notNull(final @Nullable X x, final String name) throws IllegalArgumentException {
        if (x == null) {
            throw new IllegalArgumentException(name + " is null");
        }
        return x;
    }

    public static <X> X notNull(final @Nullable X x) throws IllegalArgumentException {
        if (x == null) {
            throw new IllegalArgumentException("unexpected null value");
        }
        return x;
    }

}
