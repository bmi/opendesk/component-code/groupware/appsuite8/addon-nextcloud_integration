package com.openexchange.file.storage.nextcloud.oauth.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.annotation.Nullable;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;

public enum NextcloudResponseParser {
    ;
    
    private static final String CHARSET = StandardCharsets.UTF_8.name();
    
    public static String ocsDataId(InputStream is) throws IOException, JSONException, OXException {
        String string = Streams.stream2string(is, CHARSET);
        // assume json
        JSONObject jsonObject = new JSONObject(string);
        JSONObject ocs = jsonObject.optJSONObject("ocs");
        if (null == ocs) {
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("ocs json missing");
        }
        JSONObject data = ocs.optJSONObject("data");
        if (null == data) {
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("ocs.data json missing");
        }
        String id = data.optString("id");
        if (id == null) {
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("ocs.data.id json missing");
        }
        return id;
    }

    
    public static String ocsDataUserId(@Nullable InputStream inputStream) throws IOException, JSONException, OXException {
        String string = Streams.stream2string(inputStream, CHARSET);
        // assume json
        JSONObject jsonObject = new JSONObject(string);
        JSONObject ocs = jsonObject.optJSONObject("ocs");
        if (null == ocs) {
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("ocs json missing");
        }
        JSONObject data = ocs.optJSONObject("data");
        if (null == data) {
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("ocs.data json missing");
        }
        String userId = data.optString("userId");
        if (userId == null) {
            throw NextcloudOauthExceptionCodes.NO_USER_ID.create("ocs.data.userId json missing");
        }
        return userId;
    }

}
