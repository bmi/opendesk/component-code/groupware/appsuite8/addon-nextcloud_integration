/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.nextcloud.filepicker.osgi;

import static com.openexchange.file.storage.nextcloud.oauth.N.logger;
import java.util.Map;
import com.google.common.collect.ImmutableMap;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.ajax.requesthandler.osgiservice.AJAXModuleActivator;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.parse.FileMetadataParserService;
import com.openexchange.nextcloud.filepicker.impl.CustomAttachmentAction;
import com.openexchange.nextcloud.filepicker.impl.NextcloudHttpCLientConfigProvider;
import com.openexchange.nextcloud.filepicker.impl.NextcloudShareLinkAction;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.SpecificHttpClientConfigProvider;

@NonNullByDefault
public class NextcloudFilePickerActivator extends AJAXModuleActivator {

    private static transient final org.slf4j.Logger LOG = logger(NextcloudFilePickerActivator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            IDBasedFileAccessFactory.class,
            FileMetadataParserService.class,
            CapabilityService.class,
            HttpClientService.class,
            LeanConfigurationService.class
        };
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("starting bundle: com.openexchange.nextcloud.filepicker");
        track(CryptographicServiceAuthenticationFactory.class);
        track(CryptographicAwareIDBasedFileAccessFactory.class);

        openTrackers();

        IDBasedFileAccessFactory fileAccessFactory = getServiceSafe(IDBasedFileAccessFactory.class);
        FileMetadataParserService fileMetadataParserService = getServiceSafe(FileMetadataParserService.class);
        CapabilityService capabilityService = getServiceSafe(CapabilityService.class);
        final Map<String, AJAXActionService> actions = ImmutableMap.of(
            "attachment", new CustomAttachmentAction(fileAccessFactory, fileMetadataParserService, capabilityService, this),
            "sharelink", new NextcloudShareLinkAction(getServiceSafe(HttpClientService.class), getServiceSafe(LeanConfigurationService.class)));
        registerService(SpecificHttpClientConfigProvider.class, new NextcloudHttpCLientConfigProvider(getServiceSafe(LeanConfigurationService.class)));
        registerModule(actions::get, "nextcloud/filepicker");
    }

}
