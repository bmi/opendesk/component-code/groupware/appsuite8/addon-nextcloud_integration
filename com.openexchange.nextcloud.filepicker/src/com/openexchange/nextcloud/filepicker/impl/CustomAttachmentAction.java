/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.nextcloud.filepicker.impl;

import static com.openexchange.java.Strings.isEmpty;
import static com.openexchange.mail.mime.MimeTypes.equalPrimaryTypes;
import static com.openexchange.file.storage.nextcloud.oauth.N.notNull;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.Mail;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.annotation.Nullable;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.file.storage.parse.FileMetadataParserService;
import com.openexchange.java.Streams;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.dataobjects.MailPart;
import com.openexchange.mail.mime.ContentType;
import com.openexchange.mail.mime.MimeType2ExtMap;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

@NonNullByDefault
public class CustomAttachmentAction extends SafeAJAXActionService {

    private static final String PARAMETER_ID            = AJAXServlet.PARAMETER_ID;
    private static final String PARAMETER_FOLDERID      = AJAXServlet.PARAMETER_FOLDERID;
    private static final String PARAMETER_MAILATTCHMENT = Mail.PARAMETER_MAILATTCHMENT;

    private final IDBasedFileAccessFactory  fileAccessFactory;
    private final FileMetadataParserService fileMetadataParserService;
    private final ServiceLookup             services;
    private final CapabilityService         capabilityService;

    public CustomAttachmentAction(IDBasedFileAccessFactory fileAccessFactory, FileMetadataParserService fileMetadataParserService, CapabilityService capabilityService, ServiceLookup services) {
        super();
        this.fileAccessFactory = fileAccessFactory;
        this.fileMetadataParserService = fileMetadataParserService;
        this.capabilityService = capabilityService;
        this.services = services;

    }

    @Nullable
    private JSONObject optJSONObject(AJAXRequestData req) throws OXException {
        Object data = req.getData();
        if (null == data) {
            return null;
        }

        if (data instanceof JSONObject) {
            return (JSONObject) data;
        }

        try {
            return new JSONObject(data.toString());
        } catch (JSONException e) {
            throw AjaxExceptionCodes.INVALID_JSON_REQUEST_BODY.create(e, new Object[0]);
        }
    }

    @Override
    public AJAXRequestResult performNullChecked(AJAXRequestData req, ServerSession session) throws OXException {
        JSONObject jsonObject = optJSONObject(req);
        if (null == jsonObject) {
            throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create();
        }

        // Read parameters
        String folderPath = req.checkParameter(PARAMETER_FOLDERID);
        String uid = req.checkParameter(PARAMETER_ID);
        String sequenceId = req.checkParameter(PARAMETER_MAILATTCHMENT);
        String destFolderIdentifier = req.checkParameter(Mail.PARAMETER_DESTINATION_FOLDER);

        verifyDestFolder(destFolderIdentifier, session);
        // Get mail interface
        MailServletInterface mailInterface = null;
        try {
            mailInterface = getMailInterface(req);

            // Get mail part
            MailPart mailPart = mailInterface.getMessageAttachment(folderPath, uid, sequenceId, false);
            if (mailPart == null) {
                throw MailExceptionCode.NO_ATTACHMENT_FOUND.create(sequenceId);
            }

            // Parse file from JSON data
            File parsedFile = fileMetadataParserService.parse(jsonObject);
            if (null == parsedFile) {
                throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create();
            }
            prepareMailPart(sequenceId, mailPart, parsedFile, notNull(EnumSet.copyOf(fileMetadataParserService.getFields(jsonObject))));

            /*
             * Put attachment's document to dedicated infostore folder
             */
            IDBasedFileAccess fileAccess = notNull(fileAccessFactory.createAccess(session));

            String id = storeAttachment(req, session, destFolderIdentifier, mailPart, parsedFile.getDescription(), fileAccess);

            File fileMetadata = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);

            // File name can differ from expected filename
            String newFilename = fileMetadata.getFileName();

            // JSON response object
            JSONObject jFileData = new JSONObject(8);
            jFileData.put("mailFolder", folderPath);
            jFileData.put("mailUID", uid);
            jFileData.put("id", id);
            jFileData.put("folder_id", destFolderIdentifier);
            jFileData.put("filename", newFilename);
            return new AJAXRequestResult(jFileData, "json");
        } catch (JSONException e) {
            throw MailExceptionCode.JSON_ERROR.create(e, e.getMessage());
        } catch (RuntimeException e) {
            throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            if (null != mailInterface) {
                mailInterface.close();
            }
        }
    }

    private void verifyDestFolder(String destFolderIdentifier, ServerSession session) throws OXException {
        if (destFolderIdentifier.startsWith("nextcloud://nextcloud_oauth")) {
            if (false == capabilityService.getCapabilities(session).contains("filestorage_nextcloud_oauth")) {
                throw AjaxExceptionCodes.NO_PERMISSION_FOR_MODULE.create("custom_filepicker_upload");
            }
            return;
        } else {
            throw AjaxExceptionCodes.NO_PERMISSION_FOR_MODULE.create("custom_filepicker_upload");
        }
    }

    protected void prepareMailPart(String sequenceId, MailPart mailPart, File parsedFile, Set<Field> set) throws OXException {
        // Apply to mail part
        String mimeType = getBaseType(mailPart);
        String fileName = mailPart.getFileName();
        if (isEmpty(fileName)) {
            fileName = "part_" + sequenceId + ".dat";
        } else {
            String contentTypeByFileName = MimeType2ExtMap.getContentType(fileName, null);
            if (null != contentTypeByFileName && !equalPrimaryTypes(mimeType, contentTypeByFileName)) {
                mimeType = contentTypeByFileName;
                mailPart.getContentType().setBaseType(mimeType);
            }
        }

        // Set file name
        if (set.contains(Field.FILENAME) && !isEmpty(parsedFile.getFileName())) {
            String givenFileName = parsedFile.getFileName();
            givenFileName = givenFileName.replaceAll(Pattern.quote("/"), "_");
            mailPart.setFileName(givenFileName);
        } else {
            fileName = fileName.replaceAll(Pattern.quote("/"), "_");
            mailPart.setFileName(fileName);
        }

        /*
         * Since file's size given from mail server is just an estimation and therefore does not exactly match the file's size a
         * future file access via WebDAV can fail because of the size mismatch. Thus set the file size to 0 to make the storage
         * measure the size.
         */
        mailPart.setSize(0);
    }

    protected String storeAttachment(AJAXRequestData req, ServerSession session, String destFolderIdentifier, MailPart mailPart, @Nullable String description, IDBasedFileAccess fileAccess) throws OXException {

        // Create document meta data for current attachment
        String name = mailPart.getFileName();
        if (name == null) {
            name = "attachment";
        }
        final File file = new DefaultFile();
        file.setId(FileStorageFileAccess.NEW);
        file.setFolderId(destFolderIdentifier);
        file.setFileName(name);
        file.setFileMIMEType(mailPart.getContentType().getBaseType());
        file.setTitle(name);
        file.setFileSize(mailPart.getSize());
        List<Field> modifiedColumns = new ArrayList<Field>();
        modifiedColumns.add(Field.FILENAME);
        modifiedColumns.add(Field.FILE_SIZE);
        modifiedColumns.add(Field.FILE_MIMETYPE);
        modifiedColumns.add(Field.TITLE);
        if (null != description) {
            file.setDescription(description);
            modifiedColumns.add(Field.DESCRIPTION);
        }

        //Check for encryption
        if (AJAXRequestDataTools.parseBoolParameter(req.getParameter("encrypt"))) {
            CryptographicAwareIDBasedFileAccessFactory cryptoFileAccessFactory = services.getOptionalService(CryptographicAwareIDBasedFileAccessFactory.class);
            if (cryptoFileAccessFactory != null) {
                //encrypt the mail attachment
                EnumSet<CryptographyMode> encryptMode = EnumSet.of(CryptographyMode.ENCRYPT);
                fileAccess = notNull(cryptoFileAccessFactory.createAccess(fileAccess, encryptMode, session));
            } else {
                throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(CryptographicAwareIDBasedFileAccessFactory.class.getSimpleName());
            }
        }

        boolean retry = true;
        int count = 1;
        final StringBuilder hlp = new StringBuilder(16);
        while (retry) {
            /*
             * Get attachment's input stream
             */
            final InputStream in = mailPart.getInputStream();
            boolean rollbackNeeded = false;
            try {
                /*
                 * save attachment in storage, ignoring potential warnings due to limited storage capabilities
                 */
                fileAccess.startTransaction();
                rollbackNeeded = true;
                try {
                    fileAccess.saveDocument(file, in, FileStorageFileAccess.UNDEFINED_SEQUENCE_NUMBER, modifiedColumns, false, true, false);
                    fileAccess.commit();
                    rollbackNeeded = false;
                    retry = false;
                } catch (OXException x) {
                    fileAccess.rollback();
                    rollbackNeeded = false;
                    if (!x.isPrefix("IFO")) {
                        throw x;
                    }
                    if (441 != x.getCode()) {
                        throw x;
                    }
                    /*
                     * Duplicate document name, thus retry with a new name
                     */
                    hlp.setLength(0);
                    final int pos = name.lastIndexOf('.');
                    final String newName;
                    if (pos >= 0) {
                        newName = hlp.append(name.substring(0, pos)).append("_(").append(++count).append(')').append(name.substring(pos)).toString();
                    } else {
                        newName = hlp.append(name).append("_(").append(++count).append(')').toString();
                    }
                    file.setFileName(newName);
                    file.setTitle(newName);
                } catch (RuntimeException e) {
                    throw MailExceptionCode.UNEXPECTED_ERROR.create(e, e.getMessage());
                } finally {
                    if (rollbackNeeded) {
                        fileAccess.rollback();
                    }
                    finishSafe(fileAccess);
                }
            } finally {
                Streams.close(in);
            }
        }
        return notNull(file.getId());
    }

    protected MailServletInterface getMailInterface(AJAXRequestData req) throws OXException {
        // Requests can control whether or not to decrypt messages or verify signatures
        boolean decrypt = AJAXRequestDataTools.parseBoolParameter(req.getParameter("decrypt"));
        boolean verify = AJAXRequestDataTools.parseBoolParameter(req.getParameter("verify"));

        // Parsing crypto authentication from the request if decrypting
        String cryptoAuthentication = null;
        if (decrypt) {
            CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory = services.getOptionalService(CryptographicServiceAuthenticationFactory.class);
            if (encryptionAuthenticationFactory != null) {
                cryptoAuthentication = encryptionAuthenticationFactory.createAuthenticationFrom(req);
            }
        }

        return decrypt || verify ? notNull(MailServletInterface.getInstanceWithDecryptionSupport(req.getSession(), cryptoAuthentication)) : notNull(MailServletInterface.getInstance(req.getSession()));
    }

    private String getBaseType(MailPart mailPart) {
        return getContentType(mailPart, false);
    }

    private String getContentType(MailPart mailPart, boolean includeCharsetParameterIfText) {
        ContentType contentType = mailPart.getContentType();
        if (includeCharsetParameterIfText && contentType.containsCharsetParameter() && contentType.startsWith("text/")) {
            return notNull(new ContentType().setPrimaryType(contentType.getPrimaryType()).setSubType(contentType.getSubType()).setCharsetParameter(contentType.getCharsetParameter()).toString());
        }
        return notNull(contentType.getBaseType());
    }

    private static void finishSafe(@Nullable IDBasedFileAccess fileAccess) {
        if (fileAccess != null) {
            try {
                fileAccess.finish();
            } catch (Exception e) {
                // IGNORE
            }
        }
    }
}
