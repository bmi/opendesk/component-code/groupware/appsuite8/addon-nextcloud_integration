import $ from '$/jquery'
import _ from '$/underscore'
import moment from '$/moment'
import ox from '$/ox'
import yell from '$/io.ox/core/yell'

import { register, createShares } from '@/io.ox.nextcloud/file-picker'
import { settings } from '@/io.ox.nextcloud/settings'

const server = settings.get('server', window.location.origin + '/nextcloud')
  .replace(/\/$/, '')
const loaded = import('nextcloud-webdav-filepicker/js/filePickerWrapper')

function getMainColor () {
  const node = $('<div id="filepicker-maincolor" style="display:none">')
    .appendTo('body')
  const color = getComputedStyle(node.get(0)).color
  node.remove()
  return color
}

function open (mode, options) {
  const def = $.Deferred()
  const id = _.uniqueId('io-ox-nextcloud-')
  const container = $('<div>').attr('id', id).appendTo('body')
  const picker = window.createFilePicker(id, _.extend({
    url: server,
    themeColor: getMainColor(),
    language: ox.language.replace(/_/g, '-')
  }, options))
  $(document).on('filepicker-closed filepicker-manually-closed', function () {
    def.resolve([])
  })
  def.always(function () {
    container.remove()
    $(document).off('get-files-link get-files-path get-save-file-path')
      .off('filepicker-closed filepicker-manually-closed')
  })
  switch (mode) {
    case 'link':
      $(document).on('get-files-link', function (e) {
        const files = e.detail
        if (files.shareLinks) {
          const error = _.find(files.shareLinks, 'error')
          if (error) {
            yell(error).appendTo(document.body)
          } else {
            def.resolve(_.pluck(files.shareLinks, 'url'))
          }
        } else {
          const newOpts = {}
          const o = files.linkOptions
          if (o.protectionPassword) newOpts.password = o.protectionPassword
          if (o.allowEdition) newOpts.permissions = 3
          if (o.expirationDate) {
            newOpts.expireDate = moment(o.expirationDate).format('YYYY-MM-DD')
          }
          createShares(files.pathList, newOpts).then(def.resolve, def.reject)
        }
      })
      picker.getFilesLink()
      break
    case 'pim':
    case 'file':
      $(document).on('get-files-path', function (e) {
        def.resolve(e.detail.selection)
      })
      picker.getFilesPath()
      break
    case 'save':
      $(document).on('get-save-file-path', function (e) {
        def.resolve([e.detail.path])
      })
      picker.getSaveFilePath()
    // no default
  }
  return def.promise()
}

register({
  id: 'nextcloud',
  icon: 'io.ox.nextcloud/logo.svg',
  load: function () { return loaded },
  open
})
