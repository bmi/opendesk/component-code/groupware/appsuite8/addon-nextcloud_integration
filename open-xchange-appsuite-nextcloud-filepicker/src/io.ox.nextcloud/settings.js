import { Settings } from '$/io.ox/core/settings'

export const settings = new Settings('io.ox.nextcloud', () => ({}))
