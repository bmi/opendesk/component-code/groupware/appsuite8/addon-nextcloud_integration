import $ from '$/jquery'
import _ from '$/underscore'
import { gt } from 'gettext'
import Backbone from '$/backbone'

import Dropdown from '$/io.ox/backbone/mini-views/dropdown'
import { Action } from '$/io.ox/backbone/views/actions/util'
import Attachments from '$/io.ox/core/attachments/backbone'
import capabilities from '$/io.ox/core/capabilities'
import { buttonWithIcon, createIcon } from '$/io.ox/core/components'
import ext from '$/io.ox/core/extensions'
import http from '$/io.ox/core/http'
import yell from '$/io.ox/core/yell'
import mailApi from '$/io.ox/mail/api'
import { uploadAttachment } from '$/io.ox/mail/compose/util'

import '@/io.ox.nextcloud/style.scss'

function insertLinks (editor, urls) {
  if (editor.getMode() === 'text') {
    const textarea = editor.getContainer().get(0)
    if (textarea && 'selectionStart' in textarea) {
      const start = textarea.selectionStart
      const content = textarea.value
      const before = content.slice(0, start)
      const after = content.slice(textarea.selectionEnd)
      const newText = (before.charAt(start - 1) > ' ' ? ' ' : '') +
                      urls.join('\n') + '\n'
      textarea.value = before + newText + after
      textarea.focus()
      textarea.selectionStart = textarea.selectionEnd =
                  start + newText.length
    } else {
      editor.appendContent(' ' + urls.join('\n') + '\n')
    }
  } else { // HTML editor
    editor.tinymce().insertContent(
      urls.map(url => ` <a href="${url}">${_.escape(url)}</a><br>`).join('')
    )
  }
}

function unhex (_, hex) { return String.fromCharCode(parseInt(hex, 16)) }

const B64URL = { '+': '-', '/': '_', '=': '' }

function b64 (s) {
  return btoa(encodeURIComponent(s).replace(/%([0-9A-F]{2})/g, unhex))
    .replace(/[+/=]/g, c => B64URL[c])
}

function base64 (file) {
  const match = /^(.*\/)([^/]*)$/.exec(file)
  if (!match || !match[2]) return b64(file)
  return b64(encodeURI(match[1])) + '/' + b64(match[2])
}

function addAttachments (container, model, files, origin) {
  container.trigger('aria-live-update', gt('Added %s to attachments.', files.join(', ')))
  const models = files.map(file => {
    const id = origin + '://' + origin + '_oauth/' + base64(file)
    const folderId = id.replace(/\/[^/]*$/, '')
    const attachment = new Attachments.Model({ filename: file })
    uploadAttachment({
      model,
      filename: file,
      origin: { origin: 'drive', id, folderId },
      attachment
    })
    return attachment
  })
  model.attachFiles(models)
}

function addPIMAttachments (collection, files, origin) {
  collection.add(files.map(file => {
    const id = origin + '://' + origin + '_oauth/' + base64(file)
    const folderId = id.replace(/\/[^/]*$/, '')
    return {
      action: 'reference',
      filename: file.replace(/^.*\//, ''),
      origin: 'drive',
      origin_id: encodeURIComponent(folderId) + '/' + encodeURIComponent(id),
      origin_folder_id: folderId
    }
  }))
}

function multiple (request) {
  http.pause()
  try {
    request()
  } catch (e) {
    http.resume()
    throw e
  }
  return http.resume().then(function (response) {
    if (!_.isArray(response)) response = [response]
    const failed = _.find(response, 'error')
    if (failed) {
      yell(failed.error)
      return Promise.reject(failed.error)
    }
    return _.pluck(response, 'data')
  }, yell)
}

function saveAttachments (files, path, origin) {
  if (!_.isArray(files)) files = [files]
  return multiple(function () {
    _.each(files, function (data) {
      const folder = origin + '://' + origin + '_oauth/' +
                      b64(encodeURI(path.replace(/\/?$/, '/')))
      const params = {
        action: 'attachment',
        id: data.mail.id,
        folder: data.mail.folder_id,
        dest_folder: folder,
        attachment: data.id,
        decrypt: (data.security && data.security.decrypted)
      }
      // Guard actions.  If it was from decrypted email pass auth info
      if (data.security && data.security.authentication) {
        params.cryptoAuth = data.security.authentication
      }
      // If saving encrypted copy, but be re-encrypted from original email
      if (data.reEncrypt) params.encrypt = true
      http.PUT({
        module: 'nextcloud/filepicker',
        params,
        data: {
          folder_id: folder,
          description: gt('Saved mail attachment')
        },
        appendColumns: false
      })
    })
  }).then(function () {
    yell('success',
      // do not use "gt.ngettext" for plural without count
      (files.length === 1)
        ? gt('Attachment has been saved')
        : gt('Attachments have been saved')
    )
  })
}

ext.point('io.ox.nextcloud/file-picker/options').extend({
  // Collects file picker options in baton.data
  // Called via Point.cascade()
  perform: function (baton) {
    return http.GET({ module: 'nextcloud/accesstoken' })
      .then(function (data) { baton.data = data })
  }
})

const AttachmentNextcloudUploadView = Backbone.View.extend({

  className: 'add-from-storage mr-8',

  events: {
    'click button': 'onClick'
  },

  onClick (e) {
    e.preventDefault()
    this.open()
  },

  render () {
    this.$el.append(
      $('<button type="button" class="btn btn-default btn-file" data-action="add-internal">')
        .text(gt('Add from Files'))
    )
    return this
  }
})

export function register (options) {
  Action('io.ox.nextcloud/file-picker/actions/save', {
    collection: 'some',
    action: async baton => {
      const folder = await openFilePicker('save', baton)
      saveAttachments(baton.data, folder, options.id)
    }
  })

  ext.point('io.ox/mail/attachment/links').extend({
    id: 'nextcloud-filepicker-save',
    after: 'download',
    mobile: 'hi',
    title: gt('Save to Files'),
    ref: 'io.ox.nextcloud/file-picker/actions/save'
  })

  ext.point('io.ox/core/viewer/toolbar/links/mail').extend({
    id: 'nextcloud-filepicker-save',
    prio: 'lo',
    mobile: 'lo',
    title: gt('Save to Files'),
    ref: 'io.ox.nextcloud/file-picker/actions/save'
  })

  ext.point('io.ox/mail/compose/composetoolbar').extend({
    id: 'nextcloud-filepicker',
    after: 'add_attachments',
    draw: function (baton) {
      baton.uploadMenuView.dropdown
        .link('link', gt('Add link from Files'), openFilePicker.bind(this, 'link', baton))
        .link('add', gt('Add from Files'), openFilePicker.bind(this, 'file', baton))
    }
  })

  ext.point('io.ox/mail/compose/composetoolbar-mobile').extend({
    id: 'nextcloud-filepicker',
    after: 'add_attachments',
    draw: function (baton) {
      const node = this.find('button').last()
      const fileInput = this.find('input[type="file"]')
      const $toggle = buttonWithIcon({
        className: 'btn btn-link dropdown-toggle mr-6',
        icon: createIcon('bi/paperclip.svg').addClass('bi-22'),
        title: gt('Add attachments'),
        ariaLabel: gt('Add attachments')
      }).attr('data-toggle', 'dropdown')
      const dropdown = new Dropdown({ $toggle, title: gt('Attachments') })
      fileInput.detach()
      node.replaceWith(
        fileInput,
        dropdown.append(
          $('<a href="#">').text(gt('Add local file')).on('click', function () {
            // WORKAROUND "bug" in Chromium (no change event triggered when selecting the same file again,
            // in file picker dialog - other browsers still seem to work)
            fileInput[0].value = ''
            fileInput.trigger('click')
          })
        )
          .link('add-file', gt('Add link from Files'), openFilePicker.bind(this, 'link', baton))
          .link('add-file', gt('Add from Files'), openFilePicker.bind(this, 'file', baton))
          .render().$el
      )
    }
  })

  function addPimButton (baton) {
    const view = new AttachmentNextcloudUploadView()
    view.open = openFilePicker.bind(null, 'pim', baton.attachments)
    this.find('.attachment-list-actions').append(view.render().$el)
  }

  if (capabilities.has('filestore')) {
    ext.point('io.ox/calendar/edit/section').extend({
      id: 'io.ox.nextcloud',
      after: 'attachments_list_and_upload',
      nextTo: 'attachments_list_and_upload',
      draw: addPimButton
    })

    ext.point('io.ox/contacts/edit/view').extend({
      id: 'io.ox.nextcloud',
      after: 'attachments',
      render: function () { addPimButton.call(this.$el, this) }
    })

    ext.point('io.ox/tasks/edit/view').extend({
      id: 'io.ox.nextcloud',
      after: 'attachment_upload',
      draw: addPimButton
    })

    ext.point('io.ox/mail/actions/save-as-pdf').replace({
      id: 'default',
      capabilities: 'mail_export_pdf && filestore',
      action: saveAsPDF
    })
  }

  function hasBlockedImages (baton) {
    const threadView = baton?.threadView || baton?.app?.threadView
    if (threadView?.collection.models[0]) return threadView.collection.models[0].hasBlockedExternalImages
    const mail = baton.first()
    return mail.view === 'noimg' && mail.modified === 1
  }

  async function saveAsPDF (baton) {
    const mail = baton.first()
    const path = await openFilePicker('save', baton)
    const driveFolderId = options.id + '://' + options.id + '_oauth/' +
      b64(encodeURI(path.replace(/\/?$/, '/')))
    try {
      await mailApi.saveAsPDF({
        mailId: mail.id,
        mailFolderId: mail.folder_id,
        driveFolderId,
        includeExternalImages: !hasBlockedImages(baton),
        callback () {
          yell('info', gt('PDF file is being created. This might take a while.'))
        }
      })
      yell('success', gt('PDF file was created successfully.'))
    } catch (error) {
      console.error(error)
      yell('error', gt('Could not create PDF file.'))
    }
  }

  async function openFilePicker (mode, baton) {
    try {
      await options.load()
      const baton2 = new ext.Baton()
      await ext.point('io.ox.nextcloud/file-picker/options').cascade(this, baton2)
      const files = await options.open(mode, baton2.data)
      switch (mode) {
        case 'pim':
          return addPIMAttachments(baton, files, options.id)
        case 'link':
          return insertLinks(baton.view.editor, files)
        case 'file':
          return addAttachments(this, baton.model, files, options.id)
        case 'save':
          return files[0]
        // no default
      }
    } catch (e) {
      yell(e)
    }
  }
}

export async function createShares (files, options) {
  const response = await multiple(() => {
    _.each(files, function (file) {
      http.POST({
        module: 'nextcloud/filepicker',
        params: { action: 'sharelink' },
        data: _.extend({ path: file }, options)
      })
    })
  })
  return _.compact(_.pluck(response, 'url'))
}
